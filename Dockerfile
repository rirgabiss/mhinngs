FROM python:3.6.9-buster

WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y unzip 
RUN apt-get install -y wget
RUN apt-get install -y python3-pip

RUN wget https://bitbucket.org/rirgabiss/mhinngs/downloads/requirements.txt
RUN pip install -r requirements.txt

RUN wget https://bitbucket.org/rirgabiss/mhinngs/downloads/mhinngs_v1.0.tar.gz
RUN tar -xvf mhinngs_v1.0.tar.gz && rm mhinngs_v1.0.tar.gz

ADD https://bitbucket.org/rirgabiss/mhinngs/downloads/samtools-1.9.zip .
RUN unzip samtools-1.9.zip && rm samtools-1.9.zip
RUN wget https://bitbucket.org/rirgabiss/mhinngs/downloads/hisat2-2.1.0-source.zip 
RUN unzip hisat2-2.1.0-source.zip && rm hisat2-2.1.0-source.zip 
RUN wget https://bitbucket.org/rirgabiss/mhinngs/downloads/stringtie-2.0.3.tar.gz
RUN tar -xvf stringtie-2.0.3.tar.gz && rm stringtie-2.0.3.tar.gz
ADD https://bitbucket.org/rirgabiss/mhinngs/downloads/htslib-1.9.zip .
RUN unzip htslib-1.9.zip && rm htslib-1.9.zip

WORKDIR /usr/src/app/samtools-1.9
RUN autoreconf && ./configure && make && make install && ln -s $PWD/samtools /bin

WORKDIR /usr/src/app
RUN wget https://bitbucket.org/rirgabiss/mhinngs/downloads/tre_0.8.0.orig.tar.gz
RUN tar -xvf tre_0.8.0.orig.tar.gz && rm tre_0.8.0.orig.tar.gz
WORKDIR tre-0.8.0
RUN ./configure && make && make install && ldconfig

WORKDIR /usr/src/app/mhinngs_v1.0

CMD /bin/bash
