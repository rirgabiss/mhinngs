# MHinNGS version 1.0

MHinNGS is a tool for analysis of microhaplotypes (MHs) in singleend sequencing data obtained through a massive 
parallel sequencing plattform (MPS). 
The tool identifies the reads with the MHs and calls the genotypes of the MHs according to the criteria and positions 
specified in the configuration file.
It also searches for additional variants in the region defined by the start and the stop positions specified in the configuration file.

## Installation

#### Docker 

Retrieve the image:

	docker pull bioinformatician/mhinngs


To start a container:
```bash
docker container run --rm -it bioinformatician/MHinNGS
```

To start a container with local directory mounted to container's mhinngs directory run this instead:
```bash
docker container run -v /path/to/local/directory:/usr/src/app/mhinngs_v1.0/directory --rm -it bioinformatician/mhinngs
```

To close the container:

	exit


To remove the image:


	# Get IMAGE ID
	docker image ls

	# To remove image:
	docker image rm IMAGE_ID


#### Linux: installation from source

The following external dependencies are needed to run MHinNGS v1.0:

```
python 3.6 including pip
samtools version 1.9
hisat2 version 2.1.0
stringtie version 2.0.3
agrep T.R.E. version 0.8.0
```

To install MHinNGS:

```
# Install Python packages
wget https://bitbucket.org/rirgabiss/mhinngs/downloads/requirements.txt
pip install -r requirements.txt

# Install MHinNGS v1.0
wget https://bitbucket.org/rirgabiss/mhinngs/downloads/mhinngs_v1.0.tar.gz
tar -xvf mhinngs_v1.0.tar.gz && rm mhinngs_v1.0.tar.gz
cd mhinngs_v1.0

```

#### Test set

A test set is available in order to test the correct functionality of the program.
It contains all the input files needed to test the program: a reference genome for the test (test_ref.fa), two zipped FASTQ files (sample_1.fastq.gz and sample_2.fastq.gz) and a configuration file (test.ini). 
The test set also contains a folder named "expected_results". This contains a log file, together with the results files (results.csv and raw_results.csv). 
The test set size is ~18,3MB zipped. 

The test set is available from Bitbucket:
https://bitbucket.org/rirgabiss/mhinngs/ or download directly as:

```
wget https://bitbucket.org/rirgabiss/mhinngs/downloads/test_set.tar.gz
tar -xvf test_set.tar.gz && rm test_set.tar.gz
```


## Usage


#### To run

```
./src/MHinNGS.py -i test_set/test.fastq.gz -o ./results/test -r test_set/chr12.fa -n test_set/test.ini [optional]
```

#### Options available

```bash
-h, --help       Show options
--version        Show version

Required:
-i, --input      If only one file is analysed: input file (FASTA, FASTQ, SAM, BAM or CRAM format). If multiple files are analysed: directory where input files are located
-o, --outdir     Name of output folder
-r, --ref        Reference genome FASTA file
-c, --conf       Configuration file in *.ini format

Optional:
-t, --threads <threads>    Number of cores available for multi processing [Default: 1]
-d, --dir <dir>            If a directory is given as input, indicate the file type fomat to be analysed [Default: None]
-m, --mism <mism>          Number of extra mismatches (these will bu summed up with the number of mismatches in the configuration file) [Default: 0]
-b, --debug                This option will output extra intermediate files
-f, --force                If selected, overwriting existing output files will be allowed
```

#### Input files

Supported files: FASTA and FASTQ files containing one sample or SAM, BAM or CRAM files containing one or more samples 
identified by their ID and/or SN (SampleName) under the RG (ReadGroup). 
Multiple files may be analyzed. If multiple samples are analyzed, they must be identified by using the -d/--dir option. 
If so, please give a directory as input (-i), and write the type of file that the file format the program should search for, e.g.:
    
    -d fastq


BAM and CRAM files need corresponding index files. The file *must* be named: sample.bam.bai or sample.cram.crai. 

The program will search for both unzipped (e.g. *.fastq) and zipped (e.g. *.fastsq.gz) files.

Paired end reads are not supported.

#### Output files

**results.csv:**
File with final results after noise removal, added comments and calculations. Separated by comma.

**raw_results.csv:**
All unique sequences before noise removal.
The direction in which the sequence was found can also be seen here, along with other data.
Separated by comma.

**log_date_time.log**
Information about the run.



#### Configuration file specifications

In order to run MHinNGS, a configuration file (*.ini file) must be created, specifying information for each locus.
The configuration file contains, for each locus, 5 mandatory parameters (name, start, stop, chr and mh_info), and 15 optional parameters. 
A configuration file can be created like this:

```bash
[locus1]
start = 123456
stop = 123499
chr = 4
mh_info = rs111111:123467:CTA*AAT:AC

[locus2]
start = 456789
stop = 456810
chr = 7
mh_info = rs222222:456780:GTT*AAT:GT
```

The file *must* have unix line endings.
The locus name will be the separator between loci. The order of the parameters within each locus does not matter. 
Blank lines will be ignored and lines starting with ';' will be read as comments.

##### Mandatory parameters:

**name:**
Specify the name of the locus inside the square brackets. Each name may only appear once in the the configuration file, 
and all parameters for this locus should be placed below the name. 
All characters, except ']' are allowed in the locus name, but only letters, numbers, underscores and hyphens are recommended.

**start:**
The first position in the sequence that should be analysed (position must correspond to the reference genome used).

**stop:**
The last position in the sequence that should be analysed (position must correspond to the reference genome used).

**chr:**
Name of the chromosome where the locus is located. 
Note that the chromosome variable must match the format of selected reference genome.
If your reference looks like >1 then your configuration file should state chr = 1  
If your reference looks like >chr1 then your configuration file should state chr = chr1
Spaces are not alloved in the name.

**mh_info:**
Information of each SNP in the microhaplotype (one SNP per line).
The information (separated by a colon ':'), must contain the rs number (or a mock name if there is no rs number),
the location of the SNP in the chromosome, the recognition sequence around the SNP (minimum one base before and 
one base after the SNP), with the SNP indicated by a star '*', and the reference and alternative bases of the SNP.

One nucleotide insertion/deletion are also accepted as part of the microhaplotype.
In this case, the star should be replaced by an 'i', and the reference base should be indicated as a '-'.

Spaces before the nomenclature will be ignored. 

If a star * appears within the genotype call instead of a base, it is because the program could not  identify the recognition sequence around the SNP.
This is typically due to either insertions/deletions in the sequence or variants in the recognition sequence. 
If it is the first: increase the slide number. If the second case: change the recognition sequence in mh_info:  
e.g.:  CTA\*AAT -> A*AAT.

```
mh_info =   rs123:1551454:CTA*AATT:AC
            rs124:1551468:AA*TTG:CT
            rs125:1551472:CTiAAC:-T
			newSNP1:1551479:GTA*CCG:TG
```

##### Optional parameters:

**flank_up_length and flank_down_length:**
Number of bases after the start position and before the stop position. 
These two sequences, extracted from the reference genome, will be used to identify the locus. 
A negative number will place the bases inside the searched sequence (between start and stop position).
Default value = -15

**mism_up and mism_down:**
The number of mismatches allowed for the flank_up or flank_down. This includes substitutions, insertions, and deletions. 
*WARNING:* Runtime is exponentionally affected by the number of mismatches, so it is highly recommended to keep mismatches as low as possible.
Default value = 1

It is also possible to add mismatches from the command line when running the script (-m, --mism). 
This will be added to ALL flank mismatches in the configuration file. 
Due to the runtime, it is highly recommended to use this parameter only when testing a new population. 

**ignore_pos:**
Ignore position will replace bases in the flank with an 'N' (for SNPs) or a blank (for indels). 
A mismatch in this position will be ignored. The purpose of this option is to collapse sequences despite 
mismatches in the specified position(s).

Both single positions and intervals can be defined using integers.
For intervals, both start and stop positions will be replaced by an 'N'.
For insertions (e.g. 86386300.1T), only the indicated base (a 'T' in this example) will be ignored.

Examples:

example 1 (this will ignore 2 single positions where the last one is an insertion):

    ignore_pos = 123456789,86386300.1T  
    
example 2 (this will ignore a single position *and* an interval): 

    ignore_pos = 123456789,123456792-123456799 

When finding positions to ignore, be aware that alignments might differ:

    Multiple alignments can have the same score, and for insertions the 
    position might change due to the number of inserts:

    # Single insertion (insertion will be at the END of the poly chain)
    'AGAGGTATAAA-TAAGGATACAGATAAAGATACAAATGTTGT'
    'AGAGGTATAAAATAAGGATACAGATAAAGATACAAATGTTGT'

    # multiple insertion (insertion will be at the BEGINNING of the poly chain)
    'AGAGGTAT--AAATAAGGATACAGATAAAGATACAAATGTTGT'
    'AGAGGTATAAAAATAAGGATACAGATAAAGATACAAATGTTGT'

    Biopython’s pairwise2.align.localms used for the comparison alignment is consistent in this.

Default value = empty string

**slide:**
Slide limit is defined in order to be able to manage possible insertions/deletions around the SNPs in the microhaplotype. 
When the program tries to identify the reqognition sequence, it will slide the window, if it does *not* find a match. 
It will start in position 0, then -1, then +1 and so forth, until it finds a match, or it reaches the slide limit.
Default value = 2 [0,-1,1,-2,2]

**noise_filter:**
Noise threshold (per locus): number of reads for a unique sequence / total number of reads. 
All reads from unique sequences with read depth lower than the noise threshold will be removed from downstream analyses. Default value: 0.01

**min_reads:**
Threshold for minimum read depth. After the noise reads have been  removed, the remaining number of reads 
must exceed the threshold for minimum read depth. If the number of reads is less than this threshold, no haplotypes will be called 
and a warning flag will appear in the result file: 
“ Locus dropout” (if the read depth is 0) or “Too few reads” (if the read depth is between 1 and min_reads). Default value = 100

**max_num_unique:**
Expected number of unique sequences. 
After the noise reads have been removed, the remaining reads should correspond to a low number of unique sequences. 
If the number of unique sequences exceeds the expected number, a warning flag (“Many unique sequences”) is indicated in the result file. 
Default value = 4

**min_frac_genotype:**
Minimum threshold for genotype calling. After the noise reads have been removed, 
the haplotypes should make up a large fraction of the remaining reads (reads for genotype calling). 
If the reads of the unique sequence with most reads exceed the threshold for genotype calling, 
only one haplotype is identified and the individual is assumed to be homozygous. 
If the most numerous sequence has fewer reads than required, MHinNGS sum up the reads of the two most frequent sequences. 
If the sum exceeds the threshold for genotype calling, two haplotypes are identified and the individual is assumed to be heterozygous. 
If the sum does not exceed the threshold, MHinNGS calculates the sum of the three most frequent sequences. 
If this sum exceeds the threshold, three haplotypes are identified and a warning flag (“Three alleles”) is indicated in the result file. 
If four or more unique sequences are necessary to exceed the threshold for genotype calling, no haplotypes are identified and a warning flag 
(“More than three alleles”) is shown in the result file. Default value = 0.8

**hetero_balance:**
Test of heterozygote balance (Hb). Hb = read depth of the haplotype with most reads / read depth for both alleles. 
If Hb is outside the acceptable range, a warning flag (“Heterozygote imbalance”) is shown in the result file. Default range = 0.25-0.75

**max_reads_unique_not_called:**
Maximum read depth for unique sequences that are not identified as noise or as true alleles. 
The value S (read depth of unique sequence / read depth of genotype) is calculated for each unique sequence that is not identified as 
noise or a true haplotype. If S > max_reads_unique_not_called (M), a warning flag (“Sequence with many reads not called”) is shown in the result file. 
Default value M = 0.1 (homozygous genotypes), M / 2 (heterozygous genotypes), and M / 3 (tri-allelic genotypes). Default value = 0.1

**min_unex:**
Minimum threshold for unique sequences that are not identified as noise, true alleles, or n-1 stutters. 
A warning flag (“Unexpected sequence detected”) is indicated in the result file if a unique sequence that is not identified as noise, 
a true allele, or a n-1 stutter has a read depth>min_unex. Default value = 15

**linked_allele:**
This option can be used to ignore SNPs that are NOT included in the microhaplotype, and are in complete linkage with a microhaplotype
or one (or more) of the SNP alleles that make up the microhaplotype.
In this case, the extra SNP will not increase the information provided by the microhaplotype and it can be ignored using the "linked_allele" option.

Linked SNPs can be added to the configuration file as follows:

```
linked allele = 113393799:ACTTG:A  (location of the extra SNP: haplotype that always appears with the alternative allele of the extra SNP that is not included in the microhaplotype)

```

If linkage is only observed with one of the SNP alleles in the microhaplotype, a star '*' must replace the other SNPs in the haplotype:

```
linked allele = 113393799:***T*:A  (Linkage with only one SNP in the microhaplotype)
linked allele = 113393799:C**T*:A  (Linkage with two SNPs in the microhaplotype)
```

The following three situations for a linked SNP can be observed after running the program:

1) The indicated haplotype and alternate base of the extra SNP are observed: the extra SNP will be ignored.
2) The indicated haplotype match but the alternative base of the extra SNP does not match: the flag "Linked allele not linked" will be shown in the results file, and the position and base call will be shown in the result file.
3) The indicated haplotype does not match: the position and base of the SNP will appear in the results file, and no flag will be raised.

**rare_snp:**
If a rare SNP has been identified close to or between the SNPs of the MH, it can be called as follows: 

```
rare_snp = rs123:113393799:A   (rs_number:position:alternate base)

```

If the alternate base is observed, the comment "Rare_snp" will be shown in the output file.
In addition, the rs number will be added to the predicted allele and reported name.
If the alternate base is NOT observed, the position and base of the SNP will appear in the results file, and no flag will be raised.

#### Example of a complete configuration file (*.ini file):

```bash
[FictiveLocus]
start = 456789
stop = 456840
chr = chr7
mh_info =   rs123:1551454:CTA*AATT:AC
            rs124:1551458:AA*TTG:CT
            rs125:1551472:CTiAAC:-T

flank_up_length = 20
flank_down_length = -25
mism_up = 3
mism_down = 3
min_reads = 80
max_num_unique = 3
min_frac_genotype = 0.9
hetero_balance = 0.20-0.8
max_reads_unique_not_called = 0.15
min_unex = 20
noise_filter = 0.02
ignore_pos = 456782,456817.1T,456820-456824
linked_allele = 456800:ACTTG:A
rare_snp = rs123:456795:A
slide = 4
```

### Post analysis

To get another overview over the final results, it is possible to run a post script, to extract all predicted alleles transposed.

#### To run

```bash
./src/genotypes_for_MHinNGS.py -i /path/input -o /path/output [optional]
```


#### Options available

```bash

-h, --help    Show this screen.
--version     Show version.

Required options:
-i, --input   Must be excel or csv format
-o, --outdir  Name of output folder.

Optional options:
--both        Use run AND sample names to get unique ID. Will be separated with |.
```

#### Input file

Two types of input files are possible: csv or an excel file.
The only requirement is that the following columns must be present:
['run', 'sample', 'locus', 'PredAllele', 'Comment'], and that 'run' and
'sample' are in the first line and after each other, as this is used to determine
the separator for the csv file. Notice that column names are *case sensitive*.


#### Output files

This program will create one file, which is comma separated: 

**results_overview_w_comments.txt**

This gives an overview of predicted alleles for all loci and samples:

| Sample ID  | mh1_1    | mh1_2  | mh2_1 | mh2_2 |
|------------|----------|--------|-------|-------|
|sample_1    | GCT      | GCT    | CG    | -     |
|sample_2    | AGT#tri  | GCTtri | GG    | GC#   |
|sample_3    | ACG#     | ACT    | nd    | nd    |

* mh1_1 is microhaplotype 1, allele 1. mh1_2 is microhaplotype 1, allele 2.
* a hyphen "-" indicates that it is a homozygote. 
* hashtag [#] indicates that there is a note in the comment field.
* "tri" means that three predicted alleles were found, but only the two with most read counts are shown.
* "nd" stand for not determined, and is shown if there are too few reads.

## Author

Developed by: 

    BioInformatic Science Support (BISS)
    Copenhagen University
    Faculty of Health Sciences
    Department of Forensic Medicine
    Section of Forensic Genetics
    https://retsmedicin.ku.dk/english/
               
## Support

For comments, suggestions and support please contact Carina Jønck (carina.joenck@sund.ku.dk)

## License and citation

License can be found at:
https://bitbucket.org/rirgabiss/mhinngs/src/master/LICENSE.md



